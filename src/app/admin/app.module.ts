import { UsersModule } from './components/users/users.module';
import { ResultMessagesComponent } from './../common/result-messages/result-messages.component';
import { ResultMessageService } from './../services/result-message.service';
import { UserService } from './../services/user.service';
import { UserInfoBarComponent } from './../common/header/user-info-bar/user-info-bar.component';
import { UsersComponent } from './components/users/users.component';
import { HeaderComponent } from './../common/header/header.component';
import { SideMenuComponent } from './../common/side-menu-admin/side-menu.component';


import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { FormsComponent } from './components/forms/forms.component';
import { RouterModule, Routes } from '@angular/router';



const appRoutes: Routes = [
  { path: 'forms', component: FormsComponent },
   { path: 'users', component: UsersComponent }
]
@NgModule({
  declarations: [
    AppComponent,
    FormsComponent,
    SideMenuComponent,
    HeaderComponent,
    UserInfoBarComponent,
    ResultMessagesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    UsersModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [UserService, ResultMessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
