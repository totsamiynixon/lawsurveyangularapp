import { FormsService } from './services/forms.service';
import { FormsComponent } from './forms.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from "@angular/router/router";


@NgModule({
    imports:
    [
        FormsModule,
        CommonModule,
        //RouterModule.forChild(ProductRoutes)

    ],
    declarations:
    [   FormsComponent
    ],
    providers: [
        FormsService
    ]
})

export class ProductModule { }