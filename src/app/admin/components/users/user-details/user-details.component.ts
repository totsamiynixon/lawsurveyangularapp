import { ResultMessageService } from './../../../../services/result-message.service';
import { User } from './../../../../domain/user';
import { UserService } from './../../../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {
user: User;
JSON: JSON;
Object: Object;
  constructor(
    private aRoute: ActivatedRoute,
    private _userService: UserService,
    private _resultMessageService: ResultMessageService) {
      this.JSON = JSON;
      this.Object = Object;
     }

  ngOnInit() {
     let id = +this.aRoute.snapshot.params['id'];
     this._userService.getUser(id).subscribe(res=>{
        this.user = res;
      this._resultMessageService.setSuccessResultMessages(['Details upload successfully'])
     },error=>{
       this.user = null;
       this._resultMessageService.setErrorResultMessages(['Error while loading, please, try again']);
     })
  }

}
