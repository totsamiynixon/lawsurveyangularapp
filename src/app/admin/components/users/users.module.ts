import { UserService } from './../../../services/user.service';
import { UserDetailsComponent } from './user-details/user-details.component';
import { UsersComponent } from './users.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

export const UsersRoutes: Routes = [
    {
        path: 'users', component: UsersComponent
    },
    {
        path: 'users/details/:id', component: UserDetailsComponent
    }
]
@NgModule({
  imports: [
    CommonModule,
     RouterModule.forChild(UsersRoutes)
  ],
  declarations: [
    UsersComponent,
    UserDetailsComponent
  ],
  providers: [UserService]
})
export class UsersModule { }
