import { ResultMessageService } from './../../../services/result-message.service';
import { User } from './../../../domain/user';
import { UserService } from './../../../services/user.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  //#region "variables"
  users: Array<User>;
  //#endregion "variables"
  constructor(private _userService: UserService, private _resultMessageService: ResultMessageService) {
    this.users = null;
  }

  ngOnInit() {
    this._userService.getAllUsers().subscribe(res => {
      this.users = res;
    })
  }
  //#region "Actions"
  activate(user: User) {
  this._userService.activateUser(user.Id).subscribe(res=>{
    this._resultMessageService.setSuccessResultMessages(["User "+user.Id+" successfully activated"]);
    user.Activated = true;
  },error=>{
    this._resultMessageService.setErrorResultMessages(["User "+user.Id+" not activated"]);
  })
}
 deactivate(user: User) {
  this._userService.deactivateUser(user.Id).subscribe(res=>{
    this._resultMessageService.setSuccessResultMessages(["User "+user.Id+" successfully deactivated"]);
    user.Activated = false;
  },error=>{
    this._resultMessageService.setErrorResultMessages(["User "+user.Id+" not deactivated"]);
  })
  }
  //#endregion "Actions"
}