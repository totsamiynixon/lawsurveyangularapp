import { UserService } from './../services/user.service';
import { User } from './../domain/user';

import { Observable } from 'rxjs/Observable';
import { Component } from '@angular/core';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
user: User;
 constructor(private  _userService:UserService) {
    Observable.forkJoin(
      _userService.getCurrentUser()
    ).subscribe(res=>{
      this.user = res[0];
    })
 }
}