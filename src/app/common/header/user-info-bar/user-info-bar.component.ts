import { User } from './../../../domain/user';
import { UserService } from './../../../services/user.service';


import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-user-info-bar',
  templateUrl: './user-info-bar.component.html',
  styleUrls: ['./user-info-bar.component.scss']
})
export class UserInfoBarComponent implements OnInit {
//#region "variables"
@Input() user: User;
//#endregion "variables"
  constructor(private  _userService:UserService) { }
  
  ngOnInit() {
  }

}
