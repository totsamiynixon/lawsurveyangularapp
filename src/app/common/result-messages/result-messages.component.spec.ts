import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultMessagesComponent } from './result-messages.component';

describe('ResultMessagesComponent', () => {
  let component: ResultMessagesComponent;
  let fixture: ComponentFixture<ResultMessagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultMessagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultMessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
