import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { ResultMessageService } from './../../services/result-message.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-result-messages',
  templateUrl: './result-messages.component.html',
  styleUrls: ['./result-messages.component.scss']
})
export class ResultMessagesComponent implements OnInit {
  //#region "top"
  successMessages: Array<string>;
  warningMessages: Array<string>;
  errorMessages: Array<string>;

  errorSubscription: Subscription;
  successSubscription: Subscription;
  warningSubscription: Subscription;
  //#endregion "top"
  constructor(private _resultMessagesService: ResultMessageService) {
    this.successMessages = [];
    this.errorMessages = [];
    this.warningMessages = [];

  }

  hideAll() {
    this.errorMessages = [];
    this.successMessages = [];
    this.warningMessages = [];
  }
  ngOnInit() {
    this._resultMessagesService.getErrorResultMessages().subscribe(res => {
      if (this.errorMessages.length > 0) {
        this.errorSubscription.unsubscribe();
      }
      this.errorMessages = res;
      let endTimer = Observable.timer(5000);
      this.errorSubscription = endTimer.subscribe(t => {
        this.errorMessages = [];
      })
    }
    );
    this._resultMessagesService.getSuccessResultMessages().subscribe(res => {
      if (this.successMessages.length > 0) {
        this.successSubscription.unsubscribe();
      }
      this.successMessages = res;
      let endTimer = Observable.timer(5000);
      this.successSubscription = endTimer.subscribe(t => {
        this.successMessages = [];
      })
    }
    );

    this._resultMessagesService.getWarningResultMessages().subscribe(res => {
      if (this.warningMessages.length > 0) {
        this.warningSubscription.unsubscribe();
      }
      this.warningMessages = res;
      let endTimer = Observable.timer(5000);
      this.warningSubscription = endTimer.subscribe(t => {
        this.warningMessages = [];
      })
    });
  }
}
