export interface User{
        Id:number;
       FirstName: string;
       LastName: string;
       Email:string;
       DateCreated:Date;
       LastLoginDate:Date;
       Activated:boolean;
        StoreCredit:string;
        ForceResetPassword: boolean;
}