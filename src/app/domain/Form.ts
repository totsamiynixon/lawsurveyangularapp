export interface Form {
    Id: number;
    Slug: string;
    Title: string;
    CompanyId: number;
    Disclaimer: string;
    IsVisible: boolean;
}