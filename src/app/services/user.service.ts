import { User } from './../domain/user';

import { Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class UserService{

    categories: Observable<Array<User>>;
  private _categories: BehaviorSubject<User[]>;
  categoriesStorage: Array<User>;
  URL= '/api/ApplicationUsers/';
  constructor(private _http: Http) {
    this._categories = <BehaviorSubject<User[]>>new BehaviorSubject([]);
    this.categories = this._categories.asObservable();
    this.categoriesStorage = [];
  }
  getUser(id: number): Observable<User> {
    return this._http.get(this.URL + id).map(result => {
      return result.json();
    });
  }
  getCurrentUser(): Observable<User> {
    return this._http.get(this.URL + 'CurrentUser').map(result => {
      return result.json();
    });
  }


  getAllUsers(): Observable<Array<User>> {
    let someoneCalls = this._http.get(this.URL).map((resp) => { return resp.json() });
    
    return someoneCalls;

  }
  deleteUser(UserId: number): Observable<User> {
    let someoneCalls = this._http.delete(this.URL + UserId).map(result => {
      return result.json();
    });
    
    return someoneCalls;
  }
  addUser(User: User): Observable<User> {

    let someoneCalls = this._http.post(this.URL, User).map(result => {
      return result.json();
    });
   
    return someoneCalls;
  }

  updateUser(User: User): Observable<User> {
    let someoneCalls = this._http.put(this.URL, User).map(result => result.json());
   
    return someoneCalls;
  };
  activateUser(userId: number): Observable<any>  {
    
      let someoneCalls = this._http.put(this.URL+"ActivateUser/"+userId, userId).map(result => {
      return result.json();
    });
    return someoneCalls;
  }
   deactivateUser(userId: number): Observable<any>  {
      let someoneCalls = this._http.put(this.URL+"DeactivateUser/"+userId, userId).map(result => {
      return result.json();
    });
    return someoneCalls;
  }
}