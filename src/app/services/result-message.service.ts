import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Injectable } from '@angular/core';

@Injectable()
export class ResultMessageService {
//#region "top"

  private successMessages: Observable<Array<string>>;
  private warningMessages: Observable<Array<string>>;
  private errorMessages: Observable<Array<string>>;
  private _successMessages: BehaviorSubject<Array<string>>;
  private _warningMessages: BehaviorSubject<Array<string>>;
  private _errorMessages: BehaviorSubject<Array<string>>;
//#endregion "top"
  constructor() {
    this._successMessages = new BehaviorSubject<Array<string>>([]);
    this._errorMessages = new BehaviorSubject<Array<string>>([]);
    this._warningMessages = new BehaviorSubject<Array<string>>([]);
    this.successMessages = this._successMessages.asObservable();
    this.errorMessages = this._errorMessages.asObservable();
    this.warningMessages = this._warningMessages.asObservable();

  }


  setSuccessResultMessages(messages: Array<string>)
  {
      this._successMessages.next(messages);
  }
   setWarningResultMessages(messages: Array<string>)
   {
      this._warningMessages.next(messages);
  }
   setErrorResultMessages(messages: Array<string>){
      this._errorMessages.next(messages);
  }
  getSuccessResultMessages(): Observable<Array<string>> {
    return this.successMessages;
  }
   getWarningResultMessages(): Observable<Array<string>> {
    return this.warningMessages;
  }
   getErrorResultMessages(): Observable<Array<string>> {
    return this.errorMessages;
  }
}
