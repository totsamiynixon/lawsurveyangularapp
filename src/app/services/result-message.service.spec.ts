import { TestBed, inject } from '@angular/core/testing';

import { ResultMessageService } from './result-message.service';

describe('ResultMessageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ResultMessageService]
    });
  });

  it('should be created', inject([ResultMessageService], (service: ResultMessageService) => {
    expect(service).toBeTruthy();
  }));
});
